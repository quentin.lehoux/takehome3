﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TakeHome3
{
    [Serializable()]
    public class InscriptionException : Exception
    {
        public InscriptionException() : base() { }
        public InscriptionException(string message) : base(message) { }
        public InscriptionException(string message, System.Exception inner) : base(message, inner) { }

    }

    [Serializable()]
    public class ConnexionException : Exception
    {
        public ConnexionException() : base() { }
        public ConnexionException(string message) : base(message) { }
        public ConnexionException(string message, System.Exception inner) : base(message, inner) { }

    }

    public class AjoutBaseException : Exception
    {
        public AjoutBaseException() : base() { }
        public AjoutBaseException(string message) : base(message) { }
        public AjoutBaseException(string message, System.Exception inner) : base(message, inner) { }

    }

    public class LectureBaseException : Exception
    {
        public LectureBaseException() : base() { }
        public LectureBaseException(string message) : base(message) { }
        public LectureBaseException(string message, System.Exception inner) : base(message, inner) { }

    }
}