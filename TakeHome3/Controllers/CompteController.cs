﻿using Microsoft.AspNet.Identity;
using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TakeHome3.Controllers
{
    [Authorize]
    public class CompteController : Controller
    {
        [AllowAnonymous]
        public ActionResult Connexion(string urlRetour)
        {
            ViewBag.ReturnUrl = urlRetour;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Connexion(Modeles.Compte.Connexion connexion, string urlRetour)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    int idUtil = await connexion.Valide();
                    if (idUtil > 0)
                    {
                        var idClaim = new Claim(ClaimTypes.NameIdentifier, idUtil.ToString()  );
                        var nomClaim = new Claim(ClaimTypes.Name, connexion.Nom);
                        var claimsIdentity = new ClaimsIdentity(new[] { idClaim, nomClaim }, DefaultAuthenticationTypes.ApplicationCookie);
                        var ctx = Request.GetOwinContext();
                        var authenticationManager = ctx.Authentication;
                        authenticationManager.SignIn(new Microsoft.Owin.Security.AuthenticationProperties() { IsPersistent=false}, claimsIdentity);

                        Session["utilisateurConnecte"] = new BDD.Modeles.Utilisateur() { Identifiant = idUtil, Nom = connexion.Nom };
                        if (String.IsNullOrEmpty(urlRetour))
                            return RedirectToAction("Statistiques", "Accueil");
                        else
                            return Redirect(urlRetour);

                    }
                    else
                    {
                        ModelState.AddModelError(String.Empty, "Les indentifiants fournis ne sont pas corrects");
                    }
                }
                catch(ConnexionException conex)
                {
                    ModelState.AddModelError(String.Empty, conex.Message);
                }
            }
            return View(connexion);
        }

        [AllowAnonymous]
        public ActionResult Inscription()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Inscription(Modeles.Compte.Inscription inscription)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (await Helpers.Compte.InscriptionNouvelUtilisateur(inscription))
                        return RedirectToAction("Statistiques", "Accueil");
                    else
                        ModelState.AddModelError(String.Empty, "Impossible de réaliser l'inscription");
                }
                catch (InscriptionException iex)
                {
                    ModelState.AddModelError(String.Empty, iex.Message);
                }
            }

            // Si nous sommes arrivés là, quelque chose a échoué, réafficher le formulaire
            return View(inscription);
        }
   
        public ActionResult Deconnexion()
        {
            var ctx = Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignOut();

            // Rediriger vers la page d'accueil :
            return RedirectToAction("Connexion", "Compte");
        }
    }
}
