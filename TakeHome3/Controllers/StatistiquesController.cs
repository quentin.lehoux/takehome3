﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TakeHome3.BDD.Modeles;

namespace TakeHome3.Controllers
{
    public class StatistiquesController : Controller
    {
        [Route("Statistiques")]
        public ActionResult ObtenirDonneesUtilisateur()
        {
            dynamic resultat = null;

            Utilisateur util = Helpers.Compte.ObtenirUtilisateurConnecte(Session);
            if (util != null)
            {
                resultat = Helpers.Statistiques.ObtenirDonneesUtilisateur(util.Identifiant);
            }

            return Json(new { Donnees = resultat }, JsonRequestBehavior.AllowGet);
        }
    }
}