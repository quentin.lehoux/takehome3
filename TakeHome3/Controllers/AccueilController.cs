﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TakeHome3.Controllers
{
    [Authorize]
    public class AccueilController : Controller
    {
        public ActionResult Statistiques()
        {
            return View();
        }

        public ActionResult Cartes()
        {
            return View();
        }

        public ActionResult Operations()
        {
            return View();
        }
    }
}