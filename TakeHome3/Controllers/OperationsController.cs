﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TakeHome3.BDD.Modeles;

namespace TakeHome3.Controllers
{
    public class OperationsController : Controller
    {
        [Route("Operations")]
        public ActionResult ObtenirOperationUtilisateur()
        {
            IEnumerable<Operation> resultat = new List<Operation>();

            Utilisateur util = Helpers.Compte.ObtenirUtilisateurConnecte(Session);
            if (util != null)
            {
                resultat = Helpers.Operations.ObtenirOperationsUtilisateur(util.Identifiant);
            }

            return Json(new { Operations = resultat }, JsonRequestBehavior.AllowGet);
        }

        [Route("Operations")]
        [HttpPut]
        public ActionResult GenererOperation()
        {
            Operation resultat = null;
            Utilisateur util = Helpers.Compte.ObtenirUtilisateurConnecte(Session);
            if (util != null)
            {
                resultat = Helpers.Operations.Generer(util.Identifiant);
            }

            return Json(new { Operation = resultat });
        }
    }
}