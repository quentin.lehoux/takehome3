﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TakeHome3.BDD.Modeles;

namespace TakeHome3.Controllers
{
    [Authorize]
    public class CartesController : Controller
    {

        [Route("Cartes")]
        public ActionResult ObtenirCartes()
        {
            IEnumerable<Carte> resultat = new List<Carte>();

            Utilisateur util = Helpers.Compte.ObtenirUtilisateurConnecte(Session);
            if (util != null)
            {
                resultat = Helpers.Cartes.ObtenirCartesUtilisateur(util.Identifiant);
            }

            return Json(new { Cartes = resultat }, JsonRequestBehavior.AllowGet);
        }

        [Route("Cartes/Types")]
        public ActionResult ObtenirTypesCartes()
        {
            IEnumerable<TypeCarte> resultat = new List<TypeCarte>();

            resultat = Helpers.Cartes.ObtenirTypesCartes();

            return Json(new { Types=resultat}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("Cartes")]
        public async Task<ActionResult> AjouterCarte(Carte carte)
        {
            int resultat = 0;

            Utilisateur util = Helpers.Compte.ObtenirUtilisateurConnecte(Session);
            if (util != null)
            {
                resultat = await Helpers.Cartes.AjouterCarteUtilisateur(util.Identifiant,carte);
            }

            return Json(new { resultat }, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        [Route("Cartes/{idCarte}")]
        public ActionResult SupprimerCarte(int idCarte)
        {
            bool resultat = false;

            Utilisateur util = Helpers.Compte.ObtenirUtilisateurConnecte(Session);
            if (util != null)
            {
                resultat = Helpers.Cartes.SupprimerCarte(idCarte);
            }

            return Json(new { succes=resultat }, JsonRequestBehavior.AllowGet);
        }

    }
}