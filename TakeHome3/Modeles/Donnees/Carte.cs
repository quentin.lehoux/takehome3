﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TakeHome3.Modeles.Donnees
{
    public class Carte
    {
        public Carte() { }

        public Carte(int id,string numero) :this()
        {
            Numero = numero;
        }

        [Required]
        [Display(Name = "Numéro")]
        [DataType(DataType.CreditCard)]
        public string Numero { get; set; }

        [Display(Name = "Mois")]
        [Range(1, 12, ErrorMessage = "Le mois doit être compris entrer 1 et 12")]
        public int Mois { get; set; }

        [Display(Name = "Année")]
        [Range(2019, 2023, ErrorMessage = "L'année doit êtrer comprise entre 2019 et 2023")]
        public int Annee { get; set; }

    }
}