﻿using System.ComponentModel.DataAnnotations;

namespace TakeHome3.Modeles.Compte
{
    public class Inscription : Connexion
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Adresse email")]
        public string AdresseEmail { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} doit contenir au moins {2} caractères.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mot de passe")]
        public override string MotDePasse { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmer le mot de passe")]
        [Compare("MotDePasse", ErrorMessage = "Le nouveau mot de passe et le mot de passe de confirmation ne correspondent pas.")]
        public string ConfirmationMotDePasse { get; set; }
    }
}