﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;

namespace TakeHome3.Modeles.Compte
{
    public class Connexion
    {
        [Required]
        [Display(Name = "Nom d'utilisateur")]
        public virtual string Nom { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mot de passe")]
        public virtual string MotDePasse { get; set; }

        public virtual string MotDePasseSHA1
        {
            get
            {
                if (String.IsNullOrEmpty(MotDePasse))
                    return String.Empty;

                using (SHA1Managed sha1 = new SHA1Managed())
                {
                    return String.Concat(sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(MotDePasse)).Select(b => b.ToString("x2")));
                }
            }
        }

        public async Task<int> Valide()
        {
            return await Helpers.Compte.ConnexionUtilisateur(this);
        }
    }
}