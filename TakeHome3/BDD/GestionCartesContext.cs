﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TakeHome3.BDD.Modeles;

namespace TakeHome3.BDD
{
    public class GestionCartesContext : DbContext
    {
        public GestionCartesContext() :base()
        {
            Database.SetInitializer<GestionCartesContext>(new GestionCartesDBInitializer());
        }

        public DbSet<Utilisateur> Utilisateurs { get; set; }
        public DbSet<Carte> Cartes { get; set; }
        public DbSet<TypeCarte> TypesCartes { get; set; }
        public DbSet<Operation> Operations { get; set; }
    }

    public class GestionCartesDBInitializer : DropCreateDatabaseIfModelChanges<GestionCartesContext>
    {

        protected override void Seed(GestionCartesContext context)
        {
            base.Seed(context);
            context.TypesCartes.Add(new TypeCarte() { LibelleType = "MasterCard" });
            context.TypesCartes.Add(new TypeCarte() { LibelleType = "Visa" });

            List<Utilisateur> utilisateurs = new List<Utilisateur>()
            {
                new Utilisateur() { Nom = "Steve Rogers", AdresseEmail = "steve.rogers@avengers.marv", MotDePasse= "9cf95dacd226dcf43da376cdb6cbba7035218921"  },
                new Utilisateur() { Nom = "Tony Stark", AdresseEmail = "tony.stark@avengers.marv", MotDePasse = "9cf95dacd226dcf43da376cdb6cbba7035218921" },
                new Utilisateur() { Nom = "Natalia Alianovna Romanova", AdresseEmail = "natalia.alianovna.romanova@avengers.marv", MotDePasse = "9cf95dacd226dcf43da376cdb6cbba7035218921" },
                new Utilisateur() { Nom = "Bruce Banner", AdresseEmail = "bruce.banner@avengers.marv", MotDePasse = "9cf95dacd226dcf43da376cdb6cbba7035218921" }
            };

            context.Utilisateurs.AddRange(utilisateurs);

            context.SaveChanges();

            Random rnd = new Random();
            List<int> annees = new List<int>() { 2019, 2020, 2021, 2022, 2023 };

            foreach (Utilisateur util in utilisateurs)
            {
                List<Carte> cartes = new List<Carte>();
                int nb = rnd.Next(20)+1;

                for (int i = 0; i < nb ; i++)
                {
                    cartes.Add(new Carte()
                    {
                        Annee = annees[rnd.Next(annees.Count)],
                        Mois = rnd.Next(12) + 1,
                        Numero = Helpers.Cartes.GenererNumero(),
                        Type_Identifiant = rnd.Next(2) + 1,
                        Utilisateur = util
                    });
                    System.Threading.Thread.Sleep(rnd.Next(200));
                }

                context.Cartes.AddRange(cartes);

                context.SaveChanges();

                nb = rnd.Next(200);
                for (int i = 0; i < nb; i++)
                {
                    Carte carte = cartes[rnd.Next(cartes.Count)];

                    context.Operations.Add(new Operation()
                    {
                        Date = Helpers.Operations.DateRandom(),
                        Carte_Identifiant = carte.Identifiant,
                        Libelle = Helpers.Operations.LIBELLE_DEBIT[rnd.Next(Helpers.Operations.LIBELLE_DEBIT.Count)],
                        Montant = rnd.Next(100000)/100,
                        Sens = Operation.SensOperation.Debit
                    });
                }

                context.SaveChanges();
            }
        }
    }
}