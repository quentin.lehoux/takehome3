﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TakeHome3.BDD.Modeles
{
    public class TypeCarte
    {
        [Key]
        public int Identifiant { get; set; }
        public string LibelleType { get; set; }
    }
}