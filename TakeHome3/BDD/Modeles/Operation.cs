﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TakeHome3.BDD.Modeles
{
    public class Operation
    {
        [Key]
        public int Identifiant { get; set; }
        public DateTime Date { get; set; }
        public string Libelle { get; set; }
        public decimal Montant { get; set; }
        public SensOperation Sens { get; set; }

        [ForeignKey("Carte")]
        public int Carte_Identifiant { get; set; }
        public Carte Carte { get; set; }




        public enum SensOperation
        {      
            Credit=1,
            Debit
        }
    }
}