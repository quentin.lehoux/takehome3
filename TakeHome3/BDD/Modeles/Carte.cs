﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TakeHome3.BDD.Modeles
{
    public class Carte
    {
        [Key]
        public int Identifiant { get; set; }
 
        public string Numero { get; set; }
        public int Mois { get; set; }
        public int Annee { get; set; }

        public bool Supprimee { get; set; }

        [ForeignKey("TypeCarte")]
        public int Type_Identifiant { get; set; }
        public TypeCarte TypeCarte { get; set; }

        public Utilisateur Utilisateur { get; set; }


    }
}