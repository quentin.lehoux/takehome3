﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TakeHome3.BDD.Modeles
{
    public class Utilisateur
    {
        public Utilisateur()
        {
            Cartes = new List<Carte>();
        }

        public Utilisateur(string nom):this()
        {
            Nom = nom;
        }

        public Utilisateur(string nom, string adresseEmail) : this(nom)
        {
            AdresseEmail = adresseEmail;
        }

        [Key]
        public int Identifiant { get; set; }
        [Required]
        public string Nom { get; set; }
        public string AdresseEmail { get; set; }
        [Required]
        public string MotDePasse { get; set; }

        public ICollection<Carte> Cartes { get; set; }
    }
}