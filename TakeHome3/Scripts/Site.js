﻿function TypeCarte(id, libelle) {
    let self = this;

    self.Identifiant = ko.observable(id || -1);
    self.Libelle = ko.observable(libelle || '');
}

parseJsonDate = function (jsonDateString, avecMinutes) {
    var date = null;

    try {
        date = new Date(parseInt(jsonDateString.replace('/Date(', '')));
    }
    catch (erreur) {
        date = new Date(jsonDateString.replace('/Date(', ''));
    }

    var jour = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    var mois = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);

    var localeString = '';

    for (var i = 0; i < date.toLocaleTimeString().length; i++) { // Fix Internet Explorer
        var caractere = date.toLocaleTimeString()[i];

        if (caractere != String.fromCharCode(8206)) {
            localeString += caractere;
        }
    }

    localeString = localeString.substr(0, 5);

    date = jour + '/' + mois + '/' + date.getFullYear();

    if (avecMinutes) {
        date += ", " + localeString;
    }

    if (mois == '00') {
        date = '';
    }

    return date;
}