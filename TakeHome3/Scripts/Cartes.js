﻿function Carte(id, numero, mois,annee, type) {
    let self = this;

    self.Identifiant = ko.observable(id || 0);
    self.Numero = ko.observable(numero || '');
    self.Mois = ko.observable(mois || 1);
    self.Annee = ko.observable(annee || 19);
    self.Type_Identifiant = ko.observable(type || -1)

    self._type = ko.computed(function () {
        if (typeof vmCartes !== 'undefined') {
            if (self.Type_Identifiant() > -1) {
                return vmCartes.ObtenirTypeCarteIdentifiant(self.Type_Identifiant());
            } else {
                return 'inconnu'
            }
        }
    })
}



function ViewModelCartes() {
    let self = this;

    self.Cartes = ko.observableArray();
    self.TypesCartes = ko.observableArray();

    self._carte = ko.observable(new Carte());

    self.ChargerTypesCartes = function () {
        return new Promise(resolve => {
            $.ajax({
                type: 'Get',
                url: '/Cartes/Types',
                contentType: 'application/json',
                success: function (resultat) {
                    if (resultat) {
                        for (let i = 0; i < resultat.Types.length; i++) {
                            self.TypesCartes.push(new TypeCarte(resultat.Types[i].Identifiant, resultat.Types[i].LibelleType));
                        }
                    }
                    resolve(true);
                }
            });
        });
    }

    self.ChargerCartes = function () {
        $.ajax({
            type: 'Get',
            url: '/Cartes',
            contentType: 'application/json',
            success: function (resultat) {
                if (resultat) {
                    for (let i = 0;i< resultat.Cartes.length; i++) {
                        self.Cartes.push(new Carte(resultat.Cartes[i].Identifiant, resultat.Cartes[i].Numero, resultat.Cartes[i].Mois,resultat.Cartes[i].Annee, resultat.Cartes[i].Type_Identifiant))
                    }
                }
            }
        });
    }

    self.AjouterCarte = function () {
        if (self._carte().Numero()) {
            $.ajax({
                type: 'POST',
                url: '/Cartes',
                contentType: 'application/json',
                data: ko.toJSON({ carte: self._carte() }),
                success: function (resultat) {
                    if (resultat) {
                        self.Cartes.push(new Carte(resultat.Identifiant, self._carte().Numero(), self._carte().Mois(), self._carte().Annee(), self._carte().Type_Identifiant()))
                        self._carte(new Carte());
                    }
                    $('#popupAjoutCarte').modal('hide');
                }
            });
        } else {
            alert('Vous devez renseigner un numéro de carte');
        }
    }

    self.SupprimerCarte = function (carte) {
        if (confirm("Supprimer cette carte ?")) {
            $.ajax({
                type: 'DELETE',
                url: '/Cartes/' + carte.Identifiant(),
                success: function (resultat) {
                    if (resultat) {
                        if (resultat.succes) {
                            self.Cartes.remove(carte);
                        }
                    }
                }
            });
        }
    }

    self.ObtenirTypeCarteIdentifiant = function (id) {
        return ko.utils.arrayFirst(self.TypesCartes(),type => type.Identifiant() == id );
    }
}

$(function () {
    vmCartes = new ViewModelCartes();
    ko.applyBindings(vmCartes, document.getElementById('pageCartes'));

    vmCartes.ChargerTypesCartes().then(vmCartes.ChargerCartes);
});