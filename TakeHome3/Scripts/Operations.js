﻿function Operation(id, date, libelle, montant, sens,carte) {
    let self = this;

    self.Identifiant = ko.observable(id || 0);
    self.Date = ko.observable(date || '');
    self.Libelle = ko.observable(libelle || '');
    self.Montant = ko.observable(montant || 0);
    self.Sens = ko.observable(sens || 2)

    self._montant = ko.computed(function () {
        return self.Montant().toFixed(2);
    })
    self._carte = ko.observable(new CarteOpe(carte.Numero,carte.Type_Identifiant,carte.Supprimee));
}

function CarteOpe(num, type,suppr) {
    let self = this;

    self.Numero = ko.observable(num || '');
    self.Type_Identifiant = ko.observable(type || -1)
    self.Supprimee = ko.observable(suppr)

    self._type = ko.computed(function () {
        if (typeof vmOpes !== 'undefined') {
            if (self.Type_Identifiant() > -1) {
                return vmOpes.ObtenirTypeCarteIdentifiant(self.Type_Identifiant());
            } else {
                return 'inconnu';
            }
        }
    })
}

function ViewModelOperations() {
    let self = this;

    self.Operations = ko.observableArray();
    self.TypesCartes = ko.observableArray();

    self.ChargerOperations = function () {
        $.ajax({
            type: 'Get',
            url: '/Operations',
            contentType: 'application/json',
            success: function (resultat) {
                if (resultat) {
                    for (let i = 0; i < resultat.Operations.length; i++) {
                        self.Operations.push(new Operation(resultat.Operations[i].Identifiant, parseJsonDate(resultat.Operations[i].Date), resultat.Operations[i].Libelle, resultat.Operations[i].Montant, resultat.Operations[i].Sens,resultat.Operations[i].Carte))
                    }
                }
            }
        });
    }

    self.GenererOperation = function () {
        $.ajax({
            type: 'PUT',
            url: '/Operations',
            contentType: 'application/json',
            success: function (resultat) {
                if (resultat) {
                    if (resultat.Operation) {
                        self.Operations.push(new Operation(resultat.Operation.Identifiant, parseJsonDate(resultat.Operation.Date), resultat.Operation.Libelle, resultat.Operation.Montant, resultat.Operation.Sens, resultat.Operation.Carte))
                    }
                }
            },
            error: function (message) {
                alert('Ajoutez une carte avant de pouvoir générer une opération');
            }
        });
    }

    self.ChargerTypesCartes = function () {
        return new Promise(resolve => {
            $.ajax({
                type: 'Get',
                url: '/Cartes/Types',
                contentType: 'application/json',
                success: function (resultat) {
                    if (resultat) {
                        for (let i = 0; i < resultat.Types.length; i++) {
                            self.TypesCartes.push(new TypeCarte(resultat.Types[i].Identifiant, resultat.Types[i].LibelleType));
                        }
                    }
                    resolve(true);
                }
            });
        });
    }

    self.ObtenirTypeCarteIdentifiant = function (id) {
        return ko.utils.arrayFirst(self.TypesCartes(), type => type.Identifiant() == id);
    }
}

$(function () {
    vmOpes = new ViewModelOperations();
    ko.applyBindings(vmOpes, document.getElementById('pageOperations'));

    vmOpes.ChargerTypesCartes()
        .then(vmOpes.ChargerOperations);
});