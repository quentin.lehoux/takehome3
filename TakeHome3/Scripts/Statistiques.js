﻿function ViewModelStatistiques() {
    let self = this;

    self.NombreCartes = ko.observable(0);
    self.NombreOperations = ko.observable(0);

    self.ChargerDonnees = function () {
        $.ajax({
            type: 'Get',
            url: '/Statistiques',
            contentType: 'application/json',
            success: function (resultat) {
                if (resultat) {
                    self.NombreCartes(resultat.Donnees.NombreCartes);
                    self.NombreOperations(resultat.Donnees.NombreOperations);
                }
            }
        });
    }

    self.GenererOperation = function () {
        $.ajax({
            type: 'PUT',
            url: '/Operations',
            contentType: 'application/json',
            success: function (resultat) {
                if (resultat) {
                    if (resultat.Operation) {
                        self.Operations.push(new Operation(resultat.Operation.Identifiant, parseJsonDate(resultat.Operation.Date), resultat.Operation.Libelle, resultat.Operation.Montant, resultat.Operation.Sens, resultat.Operation.Carte))
                    }
                }
            },
            error: function (message) {
                alert('Ajoutez une carte avant de pouvoir générer une opération');
            }
        });
    }

    self.ChargerTypesCartes = function () {
        return new Promise(resolve => {
            $.ajax({
                type: 'Get',
                url: '/Cartes/Types',
                contentType: 'application/json',
                success: function (resultat) {
                    if (resultat) {
                        for (let i = 0; i < resultat.Types.length; i++) {
                            self.TypesCartes.push(new TypeCarte(resultat.Types[i].Identifiant, resultat.Types[i].LibelleType));
                        }
                    }
                    resolve(true);
                }
            });
        });
    }

    self.ObtenirTypeCarteIdentifiant = function (id) {
        return ko.utils.arrayFirst(self.TypesCartes(), type => type.Identifiant() == id);
    }
}

$(function () {
    vmStats = new ViewModelStatistiques();
    ko.applyBindings(vmStats, document.getElementById('pageStats'));

    vmStats.ChargerDonnees();
});