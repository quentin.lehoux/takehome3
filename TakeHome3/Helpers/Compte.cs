﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TakeHome3.BDD.Modeles;

namespace TakeHome3.Helpers
{
    public class Compte
    {
        public static async Task<int> ConnexionUtilisateur(Modeles.Compte.Connexion connexion)
        {
            int resultat = 0;

            using (BDD.GestionCartesContext contexte = new BDD.GestionCartesContext())
            {
                try
                {
                    BDD.Modeles.Utilisateur util = contexte.Utilisateurs.FirstOrDefault(u => u.Nom == connexion.Nom && u.MotDePasse == connexion.MotDePasseSHA1);
                    if (util != null)
                        resultat = util.Identifiant;
                }
                catch (Exception ex)
                {
                    throw new ConnexionException(ex.Message, ex);
                }
            }

            return resultat;
        }

        public static async Task<bool> InscriptionNouvelUtilisateur(Modeles.Compte.Inscription inscription)
        {
            bool resultat = false;

            using (BDD.GestionCartesContext contexte = new BDD.GestionCartesContext())
            {
                try
                {
                    contexte.Utilisateurs.Add(new BDD.Modeles.Utilisateur(inscription.Nom, inscription.AdresseEmail)
                    {
                        MotDePasse = inscription.MotDePasseSHA1
                    });

                    resultat = await contexte.SaveChangesAsync()>0;

                }
                catch (System.Data.DataException dataex)
                {
                    var errors = contexte.GetValidationErrors();
                    string erreur = errors.FirstOrDefault()?.ValidationErrors.FirstOrDefault()?.ErrorMessage;
                    throw new InscriptionException(erreur, dataex);

                }
                catch (Exception ex)
                {
                    throw new InscriptionException(ex.Message, ex);
                }
            }

            return resultat;
        }

        public static BDD.Modeles.Utilisateur  ObtenirUtilisateurConnecte(HttpSessionStateBase sessions)
        {
            Utilisateur util = sessions["utilisateurConnecte"] as Utilisateur;
            if (util == null)
            {
                using (BDD.GestionCartesContext contexte = new BDD.GestionCartesContext())
                {
                    try
                    {
                        string identity = HttpContext.Current.GetOwinContext().Authentication?.User?.Identity.GetUserId();
                        if (String.IsNullOrEmpty(identity))
                            return new Utilisateur();
                        int idUtil = int.Parse(identity);
                        util = contexte.Utilisateurs.FirstOrDefault(u => u.Identifiant== idUtil);
                        if (util != null) 
                            sessions["utilisateurConnecte"] = util;
                    }
                    catch (Exception ex)
                    {
                        throw new ConnexionException(ex.Message, ex);
                    }
                }
                
            }
            return util;
            
        }
    }
}