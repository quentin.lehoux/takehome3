﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TakeHome3.BDD.Modeles;

namespace TakeHome3.Helpers
{
    public class Statistiques
    {
        public static dynamic ObtenirDonneesUtilisateur(int idUtil)
        {
            using (BDD.GestionCartesContext contexte = new BDD.GestionCartesContext())
            {
                try
                {
                    int nbCartes = Cartes.ObtenirCartesUtilisateur(idUtil).Count();
                    int nbOpes = Operations.ObtenirOperationsUtilisateur(idUtil).Count();

                    return new { NombreCartes = nbCartes, NombreOperations = nbOpes };
                }
                catch (Exception ex)
                {
                    throw new LectureBaseException(ex.Message, ex);
                }
            }
        }
    }
}