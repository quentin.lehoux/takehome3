﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TakeHome3.BDD.Modeles;

namespace TakeHome3.Helpers
{
    public class Cartes
    {
        public static IEnumerable<Carte> ObtenirCartesUtilisateur(int idUtil)
        {
            using(BDD.GestionCartesContext contexte = new BDD.GestionCartesContext())
            {
                try
                {
                    return contexte.Cartes.Where(c => c.Utilisateur.Identifiant == idUtil && !c.Supprimee).ToList();
                    
                }
                catch (Exception ex)
                {
                    throw new LectureBaseException(ex.Message, ex);
                }
            }
        }

        public static async Task<int> AjouterCarteUtilisateur(int idUtil,Carte carte)
        {
            using (BDD.GestionCartesContext contexte = new BDD.GestionCartesContext())
            {
                try
                {
                    carte.Utilisateur = contexte.Utilisateurs.FirstOrDefault(u=>u.Identifiant == idUtil);
                    contexte.Cartes.Add(carte);

                    await contexte.SaveChangesAsync(); 
                    
                    return carte.Identifiant;
;
                }
                catch (System.Data.DataException dataex)
                {
                    var errors = contexte.GetValidationErrors();
                    string erreur = errors.FirstOrDefault()?.ValidationErrors.FirstOrDefault()?.ErrorMessage;
                    throw new AjoutBaseException(erreur, dataex);

                }
                catch (Exception ex)
                {
                    throw new AjoutBaseException(ex.Message, ex);
                }
            }
        }

        public static IEnumerable<TypeCarte> ObtenirTypesCartes()
        {
            using (BDD.GestionCartesContext contexte = new BDD.GestionCartesContext())
            {
                try
                {
                    return contexte.TypesCartes.ToList();

                }
                catch (Exception ex)
                {
                    throw new LectureBaseException(ex.Message, ex);
                }
            }
        }

        public static bool SupprimerCarte(int idCarte)
        {
            using (BDD.GestionCartesContext contexte = new BDD.GestionCartesContext())
            {
                try
                {
                    Carte carte = contexte.Cartes.FirstOrDefault(c => c.Identifiant == idCarte);
                    if (carte != null)
                        carte.Supprimee = true;

                    return contexte.SaveChanges()> 0;
                }
                catch (Exception ex)
                {
                    throw new LectureBaseException(ex.Message, ex);
                }
            }
        }

        public static string GenererNumero()
        {
            Random rnd = new Random(1);
            int i1 = (rnd.Next(9999) * DateTime.Now.Millisecond) % 10000;
            int i2 = (rnd.Next(9999) * DateTime.Now.Millisecond) % 10000;
            int i3 = (rnd.Next(9999) * DateTime.Now.Millisecond) % 10000;
            int i4 = (rnd.Next(9999) * DateTime.Now.Millisecond) % 10000;
            return $"{i1.ToString("0000")}-{i2.ToString("0000")}-{i3.ToString("0000")}-{i4.ToString("0000")}";
        }
    }
}