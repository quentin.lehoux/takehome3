﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TakeHome3.BDD.Modeles;

namespace TakeHome3.Helpers
{
    public class Operations
    {
        public static List<string> LIBELLE_DEBIT = new List<string>()
        {
            "Impôt",
            "Restaurant",
            "Kerozène",
            "Assurance",
            "Courses",
            "Achat costume neuf",
            "Changement couleurs",
            "Réparation bouclier",
            "Rapiéçage cape",
            "Réparation immeubles détruits"
        };


        public static IEnumerable<Operation> ObtenirOperationsUtilisateur(int idUtil)
        {
            using (BDD.GestionCartesContext contexte = new BDD.GestionCartesContext())
            {
                try
                {
                    List<Carte> cartes = contexte.Cartes.Where(c => c.Utilisateur.Identifiant == idUtil).ToList();
                    List<int> idCartes = cartes.Select(c => c.Identifiant).ToList();
                    var opes = contexte.Operations.Where(op => idCartes.Contains(op.Carte_Identifiant)).OrderByDescending(op => op.Date).ToList();
                    opes.ForEach(op =>
                    {
                        Carte carte = cartes.First(c => c.Identifiant == op.Carte_Identifiant);
                        op.Carte = new Carte() { Numero = "*****-" + carte.Numero.Substring(carte.Numero.Length - 4), Type_Identifiant = carte.Type_Identifiant,Supprimee =carte.Supprimee };
                    });
                    return opes;

                }
                catch (Exception ex)
                {
                    throw new LectureBaseException(ex.Message, ex);
                }
            }
        }

        public static Operation Generer(int idUtil)
        {
            Operation resultat = null;
            using (BDD.GestionCartesContext contexte = new BDD.GestionCartesContext())
            {
                try
                {
                    List<Carte> cartes = contexte.Cartes.Where(c => c.Utilisateur.Identifiant == idUtil).ToList();
                    if (cartes.Count == 0)
                        throw new Exception("Ajoutez une carte avant de pouvoir génrer des opérations");
                    
                    Random rnd = new Random();
                    Carte carte = cartes[rnd.Next(cartes.Count)];
                    
                    resultat = new BDD.Modeles.Operation()
                    {
                        Date = DateRandom(),
                        Carte_Identifiant = carte.Identifiant,
                        Libelle = LIBELLE_DEBIT[rnd.Next(LIBELLE_DEBIT.Count)],
                        Montant = rnd.Next(100000)/100,
                        Sens = Operation.SensOperation.Debit
                    };
                    contexte.Operations.Add(resultat);

                    contexte.SaveChanges();

                    resultat.Carte = new Carte() { Numero = "*****-" + carte.Numero.Substring(carte.Numero.Length - 4), Type_Identifiant = carte.Type_Identifiant };
                }
                catch (Exception ex)
                {
                    throw new AjoutBaseException(ex.Message, ex);
                }
            }
            return resultat;
        }

        public static DateTime DateRandom()
        {
            Random rnd = new Random();
            DateTime start = new DateTime(2018, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(rnd.Next(range));
        }
    }
}